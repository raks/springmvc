<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
    <div class="button">
        <a href="/logout">Log Out</a>
    </div>
  <h1>Message : ${"message"}</h1>
  <h2><%=request.getAttribute("message")%></h2>
</body>
</html>
