package com.lunchlibri.common.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AdminController {
    @RequestMapping(value = "/admin/addgoods",method = RequestMethod.GET)
    public String helloAdmin(ModelMap model){
        model.addAttribute("message", "Hello admin! Your first page!!!");
        return "admin";
    }
}
