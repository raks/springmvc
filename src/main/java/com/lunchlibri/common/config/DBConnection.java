package com.lunchlibri.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.activation.DataSource;

/**
 * Created by user on 04.05.2016.
 */
public class DBConnection {
    //@Configuration
    @Bean
    public DriverManagerDataSource getConnect(){
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/test");
        dataSource.setUsername("root");
        dataSource.setPassword("root123");
        return dataSource;
    }
}
