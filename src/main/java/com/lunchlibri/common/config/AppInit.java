package com.lunchlibri.common.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class AppInit extends AbstractAnnotationConfigDispatcherServletInitializer {
    // ���� ����� ������ ��������� ������������ ������� �������������� Beans
    // ��� ������������� ����� � ��� �������������� ��������� @Bean
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[]{
                WebConfig.class
        };
    }

    // ��� ��������� ������������, � ������� �������������� ViewResolver
    @Override
    protected Class<?>[] getServletConfigClasses() {

        /**return new Class<?>[]{
                WebConfig.class
        };**/
        return null;
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }

}
